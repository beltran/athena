/*
   Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODMUONPREPDATA_sTgcWireHitFWD_H
#define XAODMUONPREPDATA_sTgcWireHitFWD_H
#include "xAODMuonPrepData/sTgcMeasurementFwd.h"
/** @brief Forward declaration of the xAOD::sTgcWireHit */
namespace xAOD{
   class sTgcWireHit_v1;
   using sTgcWireHit = sTgcWireHit_v1;

   class sTgcWireAuxContainer_v1;
   using sTgcWireAuxContainer = sTgcWireAuxContainer_v1;
}
#endif
